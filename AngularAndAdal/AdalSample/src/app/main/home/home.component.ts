import { Component, OnInit } from '@angular/core';
import { Adal4Service } from 'adal-angular4';
import { Http, Headers, RequestOptions } from '@angular/http';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
list :any;
  constructor(private adalSvc: Adal4Service,private http :Http) { }

  ngOnInit() {
    console.log(this.adalSvc.userInfo.token);
  }

  getList(){
    
      let myHeaders = new Headers();
      myHeaders.append('Authorization',`${this.adalSvc.userInfo.token}`);
      // let options = {
      //   headers: new Headers().set('Authorization',`${this.adalSvc.userInfo.token}`)
      // };
      
      let options = new RequestOptions({ headers: myHeaders});

      // this.http.get('https://kskm1hk17k.execute-api.us-east-1.amazonaws.com/test/gethello').subscribe(response => {
      this.http.get('https://kskm1hk17k.execute-api.us-east-1.amazonaws.com/test/gethello', options).subscribe(response => {
        console.log(response);
        this.list = response;
      })
    }
}
